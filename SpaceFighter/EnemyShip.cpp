
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0)
	{   //count down until end of delay
		m_delaySeconds -= pGameTime->GetTimeElapsed();
		//once delay is over...
		if (m_delaySeconds <= 0)
		{   //activate the enemy ship
			GameObject::Activate();
		}
	}
	//if game is still running...
	if (IsActive())
	{   //once ships go off the screen, deactivate them
		m_activationSeconds += pGameTime->GetTimeElapsed();
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}